﻿using System;
using CustomAspNetIdentity.Domain.Entities;
using Microsoft.AspNet.Identity;

namespace CustomAspNetIdentity.Data.EntityFramework.Identity
{
    public class IdentityUser : IUser<string>
    {
        public IdentityUser()
        {
            Id = Guid.NewGuid().ToString();
        }

        public IdentityUser(string userName)
            : this()
        {
            UserName = userName;
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual string SecurityStamp { get; set; }
    }
}