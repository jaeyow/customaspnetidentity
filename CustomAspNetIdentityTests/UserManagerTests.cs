﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CustomAspNetIdentity.Data.EntityFramework;
using CustomAspNetIdentity.Data.EntityFramework.Identity;
using CustomAspNetIdentity.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CustomAspNetIdentityTests
{
    [TestClass]
    public class UserManagerTests
    {
        [TestMethod]
        public async Task Test_CanCreateUserWithPassword()
        {
            var userStore = new UserStore(new UnitOfWork("CustomAspNetIdentity"));
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = new IdentityUser
            {
                UserName = "johndoe@gmail.com"
            };

            var identityResult = await userManager.CreateAsync(user, "password");
            IdentityUser idUser = null;

            if (identityResult.Succeeded)
            {
                idUser = await userManager.FindByNameAsync("johndoe@gmail.com");
            }

            Assert.IsTrue(identityResult.Succeeded);
            if (idUser != null)
            {
                Assert.AreEqual("johndoe@gmail.com", idUser.UserName);
            }

            // cleanup db
            var context = new ApplicationDbContext("CustomAspNetIdentity");
            var identityUser = context.Users.FirstOrDefault(u => u.UserName == "johndoe@gmail.com");
            context.Users.Remove(identityUser);
            context.SaveChanges();
        }

        [TestMethod]
        public async Task Test_CanCreateUserWithNoPassword()
        {
            var userStore = new UserStore(new UnitOfWork("CustomAspNetIdentity"));
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = new IdentityUser
            {
                UserName = "johndoe@gmail.com"
            };

            var identityResult = await userManager.CreateAsync(user);
            IdentityUser idUser = null;

            if (identityResult.Succeeded)
            {
                idUser = await userManager.FindByNameAsync("johndoe@gmail.com");
            }

            Assert.IsTrue(identityResult.Succeeded);
            if (idUser != null)
            {
                Assert.AreEqual("johndoe@gmail.com", idUser.UserName);
                Assert.IsNull(idUser.PasswordHash);
            }

            // cleanup db
            var context = new ApplicationDbContext("CustomAspNetIdentity");
            var identityUser = context.Users.FirstOrDefault(u => u.UserName == "johndoe@gmail.com");
            context.Users.Remove(identityUser);
            context.SaveChanges();
        }

        [TestMethod]
        public async Task Test_CanChangeUserPassword()
        {
            var userStore = new UserStore(new UnitOfWork("CustomAspNetIdentity"));
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = new IdentityUser
            {
                UserName = "johndoe@gmail.com"
            };

            await userManager.CreateAsync(user, "password");

            var identityUser = await userManager.FindByNameAsync("johndoe@gmail.com");

            var identityResult = await userManager.ChangePasswordAsync(identityUser.Id, "password", "password1");

            Assert.IsTrue(identityResult.Succeeded);

            // cleanup db
            var context = new ApplicationDbContext("CustomAspNetIdentity");
            var cleaunupUser = context.Users.FirstOrDefault(u => u.UserName == "johndoe@gmail.com");
            context.Users.Remove(cleaunupUser);
            context.SaveChanges();

        }

        [TestMethod]
        public async Task Test_CanDeleteUser()
        {
            using (var userStore = new UserStore(new UnitOfWork("CustomAspNetIdentity")))
            {
                using (var userManager = new UserManager<IdentityUser>(userStore))
                {
                    var user = new IdentityUser
                    {
                        UserName = "johndoe@gmail.com"
                    };

                    var identityResult = await userManager.CreateAsync(user, "password");
                    var identityUser = await userManager.FindByNameAsync("johndoe@gmail.com");
                    identityResult = await userManager.DeleteAsync(identityUser);

                    if (identityResult.Succeeded)
                    {
                        Assert.IsTrue(identityResult.Succeeded);
                        identityUser = await userManager.FindByNameAsync("johndoe@gmail.com");
                        Assert.IsNull(identityUser);
                    }
                }
            }
        }

        [TestMethod]
        public async Task Test_CanDeleteUserAndLeaveLinkedMember()
        {
            var userStore = new UserStore(new UnitOfWork("CustomAspNetIdentity"));
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = new IdentityUser
            {
                UserName = "johndoe@gmail.com"
            };

            var context = new ApplicationDbContext("CustomAspNetIdentity");
            var identityUser = context.Users.FirstOrDefault(u => u.UserName == "johndoe@gmail.com");

            if (identityUser != null)
            {
                var idUser = new IdentityUser
                {
                    Id = identityUser.Id,
                    PasswordHash = identityUser.PasswordHash,
                    SecurityStamp = identityUser.SecurityStamp,
                    UserName = identityUser.UserName
                };

                var identityResult = await userManager.DeleteAsync(idUser);

                if (identityResult.Succeeded)
                {

                }
            }
        }
    }
}
