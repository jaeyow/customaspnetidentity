﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CustomAspNetIdentity.Data.EntityFramework;
using CustomAspNetIdentity.Data.EntityFramework.Identity;
using Microsoft.AspNet.Identity;

namespace CustomAspNetIdentity.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;

        public HomeController(UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public async Task<ActionResult>Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}