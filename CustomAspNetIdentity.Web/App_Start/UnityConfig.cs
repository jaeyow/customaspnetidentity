﻿using System.Web.Mvc;
using CustomAspNetIdentity.Data.EntityFramework;
using CustomAspNetIdentity.Data.EntityFramework.Identity;
using CustomAspNetIdentity.Domain;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;
using Unity.Mvc5;

namespace CustomAspNetIdentity.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IUnitOfWork, UnitOfWork>(new HierarchicalLifetimeManager(), new InjectionConstructor("CustomAspNetIdentity"));
            container.RegisterType<IUserStore<IdentityUser>, UserStore>(new TransientLifetimeManager());
            container.RegisterType<IRoleStore<IdentityRole, string>, RoleStore>(new TransientLifetimeManager());
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}