﻿using System;
using System.Collections.Generic;

namespace CustomAspNetIdentity.Domain.Entities
{
    public class Role
    {
        #region Fields
        private ICollection<User> _users;
        #endregion

        #region Scalar Properties
        public virtual string Id { get; set; }
        public virtual string Name { get; set; }
        #endregion

        #region Navigation Properties
            public virtual ICollection<User> Users
        {
            get { return _users ?? (_users = new List<User>()); }
            set { _users = value; }
        }
        #endregion
    }
}
